import React, { useContext,useState } from 'react'
import { useHistory } from 'react-router'

import axios from 'axios'
import url from './common'

export default function AdmLoadFile() {


let history=useHistory()
    const [file, setfile] = useState("")

    function upload(){
        
        let formData =new FormData();
        formData.append("file",file)
        axios.post(url+'/api/image/upload',formData)
    }

    return (
        <div className='pc'>
            <div className='pcLeft'>
                
               
                <div className='pcLeft5'>
                    <input type='file' onChange={(e)=>{
                        setfile(e.target.files[0])
                    }}/>
                    </div>
                    <div className='pcLeft5' onClick={upload} >上传图片</div>
                <div className='pcLeft6' onClick={
                    () => {
                        window.localStorage.removeItem('token')
                        window.localStorage.removeItem('nickname')
                        // context.setN({ nickname: '未登陆', avatar: '' })
                        history.push('/')
                    }
                }>退出登陆</div>
            </div>
        
        </div>
    )
}
