import React, { useEffect, useState, useContext } from 'react'
import axios from 'axios'
import { useParams } from "react-router-dom";
import './style.css'
import Neck from '../neck/neck'
import Right from '../right/right'
import { Context } from '../../app'
import { useHistory } from 'react-router'
import qs from 'qs'
import url from '../../common'

export default function Detaila() {

    const [result, setResult] = useState({
        avatar: '', nickname: '', level: '',
        createdAt: '', title: '', replyCount: '', viewCount: '', content: '',
        favoriteCount: 0, categoryId: '', id: ''
    })
    let id = useParams().id//拿前面传过来的ID
    
   

    const [replyList, setReplyList] = useState([])
    const [flag, setFlag] = useState(0)
    const [flagReply, setFlagReply] = useState(0)
    const [flagReply22, setFlagReply22] = useState(0)
    const [flagReply33, setFlagReply33] = useState(0)

    // --------------------------弹出登陆框
    let history = useHistory()
    let [content, setContent] = useState('')
    let [content22, setContent22] = useState('')
    let [content33, setContent33] = useState('')
    let token = window.localStorage.getItem('token')
    let [username, setUsername] = useState('')
    let [password, setPassword] = useState('')
    let [flagLogin, setFlagLogin] = useState(0)
    
    let [flagSubmit22, setFlagSubmit22] = useState(0)
    
    const context = useContext(Context)//hooks跨组件通讯

    function gotoHome() {
        let jsonData={
            username:username,
            password:password
        }
        axios.post(url+'/api/user/login',jsonData)
        .then((res) => {
         
            if (res.data.code === 'SUCCESS') {
                window.localStorage.setItem('token', res.data.token)
                window.localStorage.setItem('nickname', res.data.user.nickname)
                window.localStorage.setItem('avatar', res.data.user.avatar)
              context.setN({nickname:res.data.user.nickname,avatar:res.data.user.avatar})//hooks跨组件通讯
               
                
            } else {
               console.log(res)
               alert(res.data.message)
            }
        })
        .catch((err) => {
            console.log(err)
        }) 
    }
    // ----------------------------------------- 


    function submitReply() {
        if (token == null) {
            setFlagLogin(1)
        } else {
           
            let jsonData = {
                token: token,
                postId: id,
                replyContent: content
            }
            
            axios.post(url+'/api/post/reply', qs.stringify(jsonData) )
                .then((res) => {
                    console.log('aaa', res)
                    if (res.data.code === 'SUCCESS') {
                        
                        if (flagReply === 0) {
                            setFlagReply(1)
                        } else {
                            setFlagReply(0)
                        }
                        alert('回贴成功')
                        
                      
                        queryPost(1)
                        
                    } else {
                        alert(res.data.code)
                    }
                })
                .catch((err) => { console.log(err) })
            return () => {
            }
        }
    }
    
    function submitReply22(replyId,toUserId,postId) {
        if (token == null) {
            setFlagLogin(1)
        } else {
            
            let jsonData = {
                token: token,
                replyId: replyId,
                toUserId:toUserId,
                postId:postId,
                reply4replyContent: content22
            }
            setContent22("")
            axios.post(url+'/api/post/reply4reply', qs.stringify(jsonData) )
                .then((res) => {
                    console.log('aaa', res)
                    if (res.data.code === 'SUCCESS') {
                        if (flagReply === 0) {
                            setFlagReply22(1)
                        } else {
                            setFlagReply22(0)
                        }
                        alert('回贴成功')
                        
                       queryPost(1)
                    } else {
                        alert(res.data.code)
                    }
                })
                .catch((err) => { console.log(err) })
            return () => {
            }
        }
    }
    function submitReply33(replyId,toUserId,postId) {
        if (token == null) {
            setFlagLogin(1)
        } else {
            
            let jsonData = {
                token: token,
                replyId: replyId,
                toUserId:toUserId,
                postId:postId,
                reply4replyContent: content33
            }
            setContent33("")
            axios.post(url+'/api/post/reply4reply', qs.stringify(jsonData) )
                .then((res) => {
                    console.log('aaa', res)
                    if (res.data.code === 'SUCCESS') {
                        if (flagReply === 0) {
                            setFlagReply33(1)
                        } else {
                            setFlagReply33(0)
                        }
                        alert('回贴成功')
                        
                       queryPost(1)
                    } else {
                        alert(res.data.code)
                    }
                })
                .catch((err) => { console.log(err) })
            return () => {
            }
        }
    }
    function favoriteChange() {
        if (token == null) {
            setFlagLogin(1)
        } else {
           
            axios.post(url+'/api/post/updateMyFavoriteStatus?flag='+flag+'&postId='
                + id + '&token=' + window.localStorage.getItem('token'))
                .then((res) => {
                    queryPost(1)
                }).catch(
                    
                )
        }
    }

    // let [id, setId] = useState('')
    function queryPost(a){
        
        axios.get(url+'/api/post/find?postId=' + id+'&isReply='+a+'&token='+token)
            .then((res) => {
                console.log('aaa', res)
                if (res.data.code === 'SUCCESS') {
                    // console.log(res.data.data.postDto)
                    setResult(res.data.data)
                    setReplyList(res.data.dataList)
                    setFlag(res.data.data.status)
                  

                } else {
                    console.log(res)
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }
    useEffect(() => {
        queryPost(0)
        
        return () => {

        }
    }, [])
    return (
        <>
            <Neck></Neck>
            <div className='bodyReply'>
                <div className='left'>
                    <div className='bodyHead'>
                        <div className='bodyHead2'>
                            <div className='bodyHead21' style={{ backgroundImage: `url(${result.avatar})` }} >
                            </div>
                            <div className='bodyHead22'>
                                <div className='bodyHead221'>{result.nickname}</div>
                                <div className='bodyHead222'>{result.level}</div>
                                <div className='bodyHead223'>{result.createdAt}</div>
                            </div>
                        </div>
                        <div className='bodyHead1'>
                            <div className='bodyHead11'>标题：{result.title}</div>
                            <div style={{cursor:'pointer' }} className='bodyHead12' onClick={() => {

                                if (flagReply === 0) {
                                    setFlagReply(1)
                                } else {
                                    setFlagReply(0)
                                }
                                

                            }} >评论{result.replyCount}  --
                            浏览{result.viewCount}</div>
                        </div>
                        <div className='bodyHead3'>
                            内容： {result.content}
                        </div>
                        <div className='bodyHead5'>
                            相关标签
                            {/* {result.post.categoryId} */}
                        </div>
                        <div className='bodyHead4'>
                            <div style={{cursor:'pointer', backgroundColor:flag==0?'':'red'}} 
                            className='div1' onClick={() => {
                                if (flag == 0) {
                                    setFlag(1)
                                } else {
                                    setFlag(0)
                                }
                                
                                favoriteChange()
                            }}>收藏{result.favoriteCount}</div>--
                            <div>分享0</div>
                            --<div className='replyNum'>回复{result.replyCount}</div>
                        </div>


                    </div>
                    <div className='replyPost11' style={{ display: flagReply === 0 ? 'none' : 'block' }}>
                        <textarea onChange={(e) => {
                            setContent(e.target.value)
                            
                        }} ></textarea>
                        <div className='submit11' onClick={() => {
                            submitReply()

                        }} >提交</div>
                    </div>

                    {
                        replyList.map((post) => {
                            return (
                                <>
                                    <div key={post.id} className='reply'>
                                        <div className='reply1'>
                                            <div className='bodyHead21' style={{ backgroundImage: `url(${post.avatar})` }} >
                                            </div>

                                            <div className='replyPost'>
                                                <div className='replyPostTop'>
                                                    <div className='replyPostNickname'>
                                                        {post.nickname}
                                                    </div>--
                                                <div className='replyPostLevel'>
                                                        {post.level}
                                                    </div>
                                                </div>
                                                <div className='replyCreateAt'>{post.createdAt}
                                                </div>
                                            </div>
                                        </div>
                                        <div className='reply2'>{post.replyContent}</div>
                                        <div className='reply3'>
                                            
                                        <div style={{cursor:'pointer'}} onClick={() => {
                                               
                                                if (flagReply22 == 0) {
                                                    setFlagReply22(post.id)
                                                } else {
                                                    setFlagReply22(0)
                                                }

                                            }}>
                                                评论{post.replyReplyCount}
                                            </div>
                                        </div>
                                    </div>
                                    <div className='replyPost22' style={{ display: flagReply22 == `${post.id}` ?  'block':'none'  }}>
                                        <textarea onChange={(e) => {
                                            setContent22(e.target.value)
                                        }} ></textarea>
                                        <div className='submit22' onClick={() => {
                                            submitReply22(post.id,post.userId,post.postId)
                                        }} >提交</div>
                                    </div>




                                    {
                                        post.r4rList.map((list)=>{
                                                return(
                                                    <>
                                                    
                                    <div className='reply1111'>
                                        <div className='reply1'>
                                            <div className='bodyHead21' style={{ backgroundImage: `url(${list.avatar})` }} >
                                            </div>

                                            <div className='replyPost'>
                                                <div className='replyPostTop'>
                                                    <div className='replyPostNickname'>
                                                        {list.fromNickname}
                                                    </div>
                                                   -- 回复--
                                                    <div className='replyPostNickname'>
                                                        {list.toNickname}
                                                    </div>
                                                </div>
                                                <div className='replyCreateAt'>{list.createdAt}
                                                </div>
                                            </div>
                                        </div>
                                        <div className='reply2'>{list.reply4replyContent}</div>
                                        <div className='reply3'>
                                            
                                        <div style={{cursor:'pointer'}} onClick={() => {
                                               
                                                if (flagReply33 == 0) {
                                                    setFlagReply33(list.id)
                                                } else {
                                                    setFlagReply33(0)
                                                }

                                            }}>
                                                回复{/* 评论{list.r4rReplyCount} */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className='replyPost22' style={{ display: flagReply33 == `${list.id}` ?  'block':'none'  }}>
                                        <textarea onChange={(e) => {
                                            setContent33(e.target.value)
                                        }} ></textarea>
                                        <div className='submit22' onClick={() => {
                                            submitReply33(post.id,post.userId,post.postId)
                                        }} >提交</div>
                                    </div>
                                                    </>
                                                )
                                        })
                                    }
                          
                                











                                </>
                            )

                        })
                    }
                    {/* <div className='bodyDown'>
                        <div className='paging'>1</div>
                    </div> */}

                </div>
                <Right></Right>
            </div>
            <div className='aaaaa' style={{ display: flagLogin === 0 ? 'none' : 'block' }}>
                <div className='loginWay'>
                    <div className='loginWay1'>密码登录</div>
                    <div className='loginWay2'>邮箱登录</div>
                    <div className='loginWay3'>扫码登录</div>
                </div>
                <div className='userName'>
                    <div className='userName1'>用户名</div>
                    <div className='userName2'>
                        <input placeholder='请输入您的用户名' onChange={(e) => {
                            setUsername(e.target.value)
                        }} ></input>
                    </div>
                    <div className='userName3'>使用第三方账号注册过的用户需先绑定手机/邮箱号</div>
                </div>
                <div className='password'>
                    <div className='password1'>密码</div>
                    <div className='password2'>
                        <input placeholder='请输入6到16位的密码' onChange={(e) => {
                            setPassword(e.target.value)
                        }} ></input>
                    </div>
                    <div className='password3'>如果您之前未设置过密码，
                请使用其他方式登陆后在账户中心设置密码/邮箱号</div>
                </div>
                <div>
                    <div className='submit' onClick={
                        gotoHome
                    } >提交</div>
                    <div>尚无账号？点击此处去注册</div>
                </div>
                <div>或者使用社交账号注册</div>
            </div>

        </>)

}
