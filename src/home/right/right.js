import React, { useEffect, useState, useContext } from 'react'
import axios from 'axios'
import './style.css'
import url from '../../common'



export default function Right() {


    const [result, setresult] = useState([])

    useEffect(() => {
        axios.get(url+'/api/user/recentUser')
            .then((res) => {
                console.log('aaa=', res)
               setresult(res.data.result)
                    // console.log(res.data.data.postDto)
               
            })
            .catch((err) => {
                console.log(err)
            })
        return () => {
           
        }
    }, [])
    return (
        <div className='right'>
            <div className='rightTop'>
                <div className='rightTop1'>热门话题</div>
                <div className='rightTop2'>
                    <div>教程</div>
                    <div>公告</div>
                    <div>笔记</div>
                    <div>闲聊</div>
                </div>
            </div>
            <div className='rightMiddle'>
            <div>最近访客</div>
                <div className='rightMiddle2'>
                   
                    {
                        result.map((user)=>{
                            return(
                                <div className='abc'>
<div className='bodyMiddlePic' style={{backgroundImage:`url(${user.avatar})`}} > </div>
<div  >{user.nickname} </div>
<div  >{user.logintime} </div>
</div>

                            )
                        })
                    }


                    
               
                </div>
            </div>
            <div className='rightDown'>

                <div className='rightDown1'>点击图片或者扫码加社区交流群~</div>
                <div className='rightDown2'>二维码</div>
            </div>
        </div>
    )
}
