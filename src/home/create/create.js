import axios from 'axios'
import React, { useState, useContext } from 'react'
import './style.css'
import { useHistory } from 'react-router'
import { Context } from '../../app'
import qs from 'qs'
import url from '../../common'

import Login from '../../top/login/logonNoRedirect'

export default function Create() {
    let [title, setTitle] = useState('')
    let [content, setContent] = useState('')
    let token = window.localStorage.getItem('token')
    let history = useHistory()
    let [flag, setFlag] = useState(0)
    // --------------------------弹出登陆框

    // ----------------------------------------- 

    function createPost() {
        if (token == null) {
            setFlag(1)
        } else {
            let jsonData={
                title:title,
                content:content,
                token:token
            }
            axios.post(url+'/api/post/create',qs.stringify(jsonData) )
                .then((res) => {
                   console.log(res)
                    if (res.data.code === 'SUCCESS') {
                        alert('发布成功')
                        history.push('/')
                    } else {
                        alert(res.data.message)
                    }
                })
                .catch((err) => { console.log(err) })
            return () => {
            }
        }
    }
    return (<>
        <div className='createNew' >
            <div className='createPPost'>
                <div className='createPost1'>发表新帖</div>
            </div>
            <div className='title'>
                <div className='title1'>标题</div>
                <div className='title2'>
                    <input onChange={(e) => {
                        setTitle(e.target.value)
                    }} ></input>
                </div>
            </div>
            <div className='content'>
                <textarea onChange={(e) => {
                    setContent(e.target.value)
                }} ></textarea>
            </div>
            <div className='label'>添加标签</div>
            <div className='labelInput'><input></input></div>
            <div className='special'>
                <div className='special1'>所在专栏</div>
                <div className='specia2'>
                    <select>
                        <option>请选择</option>
                    </select>
                </div>
            </div>
            <div className='authority'>
                <div className='authority1'>阅读权限</div>
                <div className='authority2'>
                    <select>
                        <option>默认，所有用户可见</option>
                    </select>
                </div>
                <div className='authority3'>除非有重要信息，不然建议设置所有用户可见，
                    这样不会影响您帖子的人气，点击查看用户权限</div>
            </div>
            <div className='pulish' onClick={
                createPost
            } >立即发布</div>
        </div>
        <div  style={{ display: flag === 0 ? 'none' : 'block' }}>
        <Login/>
        </div>
       
    </>
    )
}
