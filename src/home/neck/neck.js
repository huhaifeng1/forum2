import React from 'react'
import './style.css'
import { Link } from 'react-router-dom'


export default function Neck() {
    //let token = window.localStorage.getItem('token')
    //console.log('aa',token)
    return (
        <div className='neck'>
            <div className='neckLeft'>
                <div>首页</div>
                <div>提问</div>
                <div>分享</div>
                <div>建议</div>
                <div>讨论</div>
                <div>公告</div>
                <div>动态</div>
                <div>其他</div>
            </div>
            <div className='neckRight'>
                <div className='neckRight1'></div>
                <div className='neckRight2'>
                    <Link className='link' to='/post/create' >发表新帖</Link>
                </div>
            </div>
        </div>
    )
}
