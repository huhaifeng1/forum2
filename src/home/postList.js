import React, { useEffect, useState } from 'react'
import './style.css'
import axios from 'axios'
import {Link} from 'react-router-dom'
import Neck from  './neck/neck'
import Right from './right/right'
import url from '../common'

export default function Home() {

    let [results, setResults] = useState([])
    let [page, setPage] = useState(1)
    let [totalPage,setTotalPage]=useState(0)
    let [limit,setLimit]=useState(5)
    let [begin,setBegin]=useState(0)
    let [end,setEnd]=useState(7)
  

    useEffect(() => {
        axios.get(url+'/api/post/posts')
            .then((res) => {
                console.log(res)
                if (res.data.code === 'SUCCESS') {
                    setResults(res.data.data)
                   //setTotalPage(res.data.data.postSearchDto.total)
                } else {
                    console.log(res)
                }
            })
            .catch((err) => {
                console.log(err)
            })
        return () => {

        }
    }, [page,limit])

    function showPage(){
        let count= Math.ceil(totalPage/limit)
        let arr=[]
        for(let i=1;i<=count;i++){
            arr.push(<div onClick={(e)=>{
                
                //console.log('i=',e.target.innerHTML)
               // console.log(typeof(parseInt(e.target.innerHTML)))
                setPage(parseInt(e.target.innerHTML))
            }} key={i} className={i===page?'active':'paging'}>{i}</div>)
        }
       let arr1=arr.slice(begin,end)
       
        return (
            <>
                <div>共{totalPage}条</div>
                <div onClick={()=>{
                    if(page>1){ setPage(page-1)
                        if(page>4&&page==end-3){
                           // console.log(begin,page,end)
                            setBegin(begin-1)
                            setEnd(end-1)
                        }}
                }}>向前</div>
                {arr1}
                {/* 当page 比较 totalPage第一条或者是最后一条的时候，按钮变灰色 */}
                <div onClick={()=>{
                    if(page<end&&page<count){ setPage(page+1)
                        if(page==begin+4&&page<=arr.length-4){
                          //  console.log(begin,page,end)
                            setBegin(begin+1)
                            setEnd(end+1)
                        }}
                   
                }} >向后</div>
                <div>
                    <select onChange={(e)=>{
                        setLimit(parseInt(e.target.value))
                        setPage(1)
                    }} >
                        <option value='5'>每页5条</option>
                        <option value='10'>每页10条</option>
                    </select>
                </div>
                <div>
                    跳至<input onKeyDown={(e)=>{
                        if(e.code==='NumpadEnter'||e.code==='Enter'){
                            if(0<parseInt(e.target.value)&&
                            parseInt(e.target.value)<=arr.length){
                                setPage(parseInt(e.target.value))

                                if(parseInt(e.target.value)<=4){
                                    setBegin(0)
                                    //console.log(begin,end)
                                    if(count>7){
                                        
                                        setEnd(7)
                                    }else{
                                        setEnd(count)
                                    }
                                }else{
                                    if(count<7){
                                      setBegin(0)
                                      setEnd(count)
                                    }else{
                                        if(parseInt(e.target.value)>arr.length-4){
                                            setBegin(arr.length-7)
                                            setEnd(arr.length)
                                        }else{
                                            setBegin(parseInt(e.target.value)-4)
                                            setEnd(parseInt(e.target.value)+3)
                                        }
                                    }
                                  
                                    
                                   // console.log(begin,parseInt(e.target.value),end)
                                }
                            }

                            
                        }
                    }} ></input>页
                </div>
            </>
        )
    }
    return (
        <>
        <Neck></Neck>
      
        <div className='bodyList'>
                <div className='left'>
                    <div className='hat'>
                        欢迎你的到来~
                    </div>
                    <div className='body2'>
                        <div className='bodyHead'>
                            <div className='bodyHead1'>
                                <div>综合</div>
                                <div className='column'></div>
                                <div>周榜</div>
                                <div className='column'></div>
                                <div>月榜</div>
                                <div className='column'></div>
                                <div>精华</div>
                            </div>
                            <div className='bodyHead2'>
                                <div>按最新</div>
                                <div className='column'></div>
                                <div>抢沙发</div>
                            </div>
                        </div>
                        {
                            results.map((post) => {
                               // console.log(post)
                                return(
                                    <div className='bodyMiddle' key={post.id}>
                                    <div className='bodyMiddlePic' style={{backgroundImage:`url(${post.avatar})`}} >
                                </div>
                                    <div className='post'>
                                        <div className='bodyMiddleTitle'>
                                            <Link className='link' to={`/post/detail/${post.id}`}>{post.title}</Link>
                                            //把ID传给后面，并且要在路由器配置一下
                                            </div>
                                        <div className='titleDown'>
                                            <div className='titleDown1'>
                                                <div className='label1'>{post.nickname}</div>
                                                <div className='label2'>{post.level}</div>
                                                <div className='label3'>{post.createdAt}</div>
                                                <div className='label4'>
                                                    
                                                   <div className='div1'></div>
                                                   <div className='div2'>{post.favoriteCount}</div>
                                                   </div>
                                            </div>
                                            <div className='titleDown2'>
                                                <div className='label5'>
                                                <div className='div1'></div>
                                                   <div className='div2'>{post.viewCount}</div>
                                                    </div>       
                                                <div className='label6'>
                                                <div className='div1'></div>
                                                   <div className='div2'>{post.replyCount}</div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                )
                
                            })
                        }
                        <div className='bodyDown'>
                           {/* {showPage()}  */}
                        </div>
                    </div>
                </div>
                {/* <Right></Right> */}
            </div>
            </>
    )
}
