import React from 'react'
import {  Switch, Route } from 'react-router-dom'

import MyPost from './myPost/myPost'
import YyFavorite from './myFavorite/myFavorite'

export default function pcApp() {
        return (            
          <Switch>
                   <Route  path='/personCenter/myPost' component={MyPost}></Route>
                   <Route  path='/personCenter/myFavorite' component={YyFavorite}></Route>
         </Switch>            
        )
}
