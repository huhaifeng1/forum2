import axios from 'axios'
import React, { useEffect, useState } from 'react'
import './style.css'
import {Link} from 'react-router-dom'
import url from '../../../common'

export default function MyFavorite() {

    let token = window.localStorage.getItem('token')
    const [results, setResults] = useState([])
    let [page, setPage] = useState(1)
    let [totalPage,setTotalPage]=useState(0)
    let [limit,setLimit]=useState(20)
    let [begin,setBegin]=useState(0)
    let [end,setEnd]=useState(7)

    function deletePost(postId){
        alert(postId)
        axios.put(url+'/api/post/deleteMyPost/'+postId+'?token='+token)
            .then((res) => {
                
                if (res.data.code === 'SUCCESS') {
                    
                    getPost()
                    // setTotalPage(res.data.data.postSearchDto.total)
                } else {
                    return
                }
            })
           
    }
    function getPost(){
        axios.get(url+'/api/post/myFavotitePosts?token=' + token + '&limit='+limit+'&page=' + page)
        .then((res) => {
            console.log("aaaaaaaaares=",res)
            if (res.data.code === 'SUCCESS') {
                
                setResults(res.data.data)
                // setTotalPage(res.data.data.postSearchDto.total)
            } else {
                return
            }
        })
        .catch((err) => {
            console.log(err)
        })
    }
    useEffect(() => {
       getPost()
        return () => {

        }
    }, [page,limit])
    function showPage(){
        let count= Math.ceil(totalPage/limit)
        console.log('count=',totalPage)
        let arr=[]
        for(let i=1;i<=count;i++){
            arr.push(<div onClick={(e)=>{
                
                //console.log('i=',e.target.innerHTML)
               // console.log(typeof(parseInt(e.target.innerHTML)))
                setPage(parseInt(e.target.innerHTML))
            }} key={i} className={i===page?'active':'paging'}>{i}</div>)
        }
        console.log('aaaa=',arr.length)
       let arr1=arr.slice(begin,end)
       
        return (
            <>
                <div>共{totalPage}条</div>
                <div onClick={()=>{
                    if(page>1){ setPage(page-1)
                        if(page>4&&page==end-3){
                           // console.log(begin,page,end)
                            setBegin(begin-1)
                            setEnd(end-1)
                        }}
                }}>向前</div>
                {arr1}
                {/* 当page 比较 totalPage第一条或者是最后一条的时候，按钮变灰色 */}
                <div onClick={()=>{
                    if(page<end&&page<count){ setPage(page+1)
                        if(page==begin+4&&page<=arr.length-4){
                          //  console.log(begin,page,end)
                            setBegin(begin+1)
                            setEnd(end+1)
                        }}
                   
                }} >向后</div>
                <div>
                    <select onChange={(e)=>{
                        setLimit(parseInt(e.target.value))
                        setPage(1)
                    }} >
                        <option value='5'>每页5条</option>
                        <option value='10'>每页10条</option>
                    </select>
                </div>
                <div>
                    跳至<input onKeyDown={(e)=>{
                        if(e.code==='NumpadEnter'||e.code==='Enter'){
                            if(0<parseInt(e.target.value)&&
                            parseInt(e.target.value)<=arr.length){
                                setPage(parseInt(e.target.value))

                                if(parseInt(e.target.value)<=4){
                                    setBegin(0)
                                    //console.log(begin,end)
                                    if(count>7){
                                        
                                        setEnd(7)
                                    }else{
                                        setEnd(count)
                                    }
                                }else{
                                    if(count<7){
                                      setBegin(0)
                                      setEnd(count)
                                    }else{
                                        if(parseInt(e.target.value)>arr.length-4){
                                            setBegin(arr.length-7)
                                            setEnd(arr.length)
                                        }else{
                                            setBegin(parseInt(e.target.value)-4)
                                            setEnd(parseInt(e.target.value)+3)
                                        }
                                    }
                                  
                                    
                                   // console.log(begin,parseInt(e.target.value),end)
                                }
                            }

                            
                        }
                    }} ></input>页
                </div>
            </>
        )
    }
    return (
        <>
            {
                results.map((result) => {
                    return (
                        <div key={result.id} className='pcRightBody'>
                            <div className='pcRightBody1'>
                                <div>
                                <Link className='link' to={'/post/detail/'+result.id}>{result.title}</Link>
                                    </div>
                                <div>{result.createdAt}</div>
                            </div>
                            <div className='pcRightBody2'>
                                <div>{result.replyCount}个回复</div>
                                <div>{result.viewCount}次浏览</div>
                                <div>{result.favoriteCount}人收藏</div>
                                <div onClick={
                                    ()=>{
                                        deletePost(result.id)
                                    }
                                } style={{cursor:'pointer'}}>删除</div>
                            </div>
                        </div>
                    )
                })
            }
             <div className='pcRightFootPage'>
                    {/* {showPage()} */}
                </div>
        </>
    )
}