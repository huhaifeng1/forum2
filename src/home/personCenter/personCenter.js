import React, { useContext,useState } from 'react'
import { useHistory } from 'react-router'
import './style.css'
import { Context } from '../../app'
import MyPost from './myPost/myPost'
import YyFavorite from './myFavorite/myFavorite'
import MyFavorite from './myFavorite/myFavorite'
import axios from 'axios'
import url from '../../common'

export default function PersonCenter() {

    const context = useContext(Context)
    let history = useHistory()

    let [flag,setFlag]=useState(0)

    const [file, setfile] = useState("")

    function upload(){
        
        let formData =new FormData();
        formData.append("file",file)
        axios.post(url+'/api/image/upload',formData)
    }

    return (
        <div className='pc'>
            <div className='pcLeft'>
                <div className='pcLeft1'>我的主页</div>
                <div className='pcLeft2'>账号中心</div>
                <div className='pcLeft3'>我的消息</div>
                <div className='pcLeft4'>帖子管理</div>
                <div className='pcLeft5' onClick={upload} >上传图片</div>
                {/* <div className='pcLeft5'>
                    <input type='file' onChange={(e)=>{
                        setfile(e.target.files[0])
                    }}/>
                    </div>
                    */}
                <div className='pcLeft6' onClick={
                    () => {
                        window.localStorage.removeItem('token')
                        window.localStorage.removeItem('nickname')
                        context.setN({ nickname: '未登陆', avatar: '' })
                        history.push('/')
                    }
                }>退出登陆</div>
            </div>
            <div className='pcRight'>
                <div className='pcRightHead'>
                    <div className='pcRightHead1'  id={flag===0?'pcActive':''} onClick={()=>{
                       flag===1?setFlag(0):setFlag(1)
                    }}  >我发的贴</div>
                    <div className='pcRightHead2' id={flag===1?'pcActive':''} onClick={()=>{
                       flag===0?setFlag(1):setFlag(0)
                    }} >收藏的贴</div>
                </div>
               {flag===0?<MyPost/>: <MyFavorite/>}
                
            </div>
        </div>
    )
}
