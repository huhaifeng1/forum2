import React from 'react'
import {  Switch, Route } from 'react-router-dom'


import Detail from './detail/detail'
import './style.css'

import Create from './create/create'

export default function Home() {
        return (
              
          <Switch>
                   <Route  path='/post/detail/:id' component={Detail}></Route>//配置页面传参数ID
                   <Route  path='/post/create' component={Create}></Route>

         </Switch>
               
        )
}
