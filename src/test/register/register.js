import axios from 'axios'
import React, { Component } from 'react'
import './style.css'
import qs from 'qs'


export default class Register extends Component {
    state={
        phoneNum:'',
        verifyCode:'',
        url:'',
        verifyPic:'',
        key:'',
        password:'',
        verify:'',
        time:10
    }
    componentWillUnmount(){
        this.setState=(state,callback)=>{//清理state
            return
        }
    }
    loadVerify=()=>{
        let data={
            mobile:this.state.phoneNum,
            captcha_code:this.state.verifyCode,
            captcha_key:this.state.key
        }
        axios.post('http://playground.it266.com/api/sms/verify',qs.stringify(data) )
        .then((res)=>{
           
             if(!res.data.status){
                 alert(res.data.data)
             }else{
                alert(res.data.data)
             }
        })
        .catch((err)=>{
             console.log(err)
        })
    }
    loadPic=()=>{
       axios.get('http://playground.it266.com/api/captcha')
       .then((res)=>{
          
            if(!res.data.status){
                alert(res.data.data)
            }else{
                this.setState({
                    url:res.data.data.url,
                    key:res.data.data.key
                })
            }
       })
       .catch((err)=>{
            console.log(err)
       })
    }
    register=()=>{
        let data={
            mobile:this.state.phoneNum,
            verify:this.state.verify,
            password:this.state.password
        }
        axios.post('http://playground.it266.com/api/user/register',qs.stringify(data) )
        .then((res)=>{
           console.log(res)
             if(!res.data.status){
                 alert(res.data.data)
             }else{
                 let token=res.data.data.token
                 let id=res.data.data.id
                window.localStorage.setItem('token',token)
                window.localStorage.setItem('id',id)
                alert('注册成功')
               this.props.history.push('/person')
             }
        })
        .catch((err)=>{
             console.log(err)
        })
    }
    render() {
        return (
            <div className='Login' style={{backgroundColor:this.state.url===''?'white':'rgb(101, 101, 101)'}}>
                <div className='verifyPic' style={{display:this.state.url===''?"none":"block"}}>
                    <div className='verifyPic1'>安全验证</div>
                    <div className='verifyPic2'>
                        <img className='verifyPic21' src={this.state.url} alt=''></img>
                        <div className='verifyPic22'
                        onClick={this.loadPic}
                        >换一换</div>
                    </div>
                    <div className='verifyPic3'>
                        <input placeholder='请输入验证码' value={this.state.verifyCode} onChange={(e)=>{
                            this.setState({verifyCode:e.target.value})
                        }}></input>
                    </div>
                    <div className='verifyPic4' onClick={()=>{
                        
                       this.loadVerify()
                       this.setState({url:''})
                       let timer=setInterval(() => {
                           this.setState({time:this.state.time-1})
                            if(this.state.time===-1){
                                clearInterval(timer)
                            }
                       }, 1000);
                    }}>确认</div>
                </div>

                <div className='head'>
                    <span className='span1'>&lt;</span>
                    <span className='span2'>完善手机号</span>
                </div>
                <div className='instruction'>为了获得更好体验，建议您立即完善手机号</div>
                <div className='region'>
                    <span className='span1'>手机号归属地</span>
                    <span className='span2'>中国&gt;</span>
                </div>
                <div className='phoneNum'>
                    <span className='span1'>+86</span>
                    <span className='span2'>
                        <input placeholder='输入手机号' vlaue={this.state.verifyCode} onChange={(e)=>{
                            this.setState({phoneNum:e.target.value})
              
                            if(
                             (/^1[3456789]\d{9}$/).test(e.target.value)&&e.target.value.length===11
                            ){
                                this.loadPic()
                          
                        }else{
                            if(e.target.value.length===11){
                                document.getElementsByClassName('error1')[0].innerHTML='请输入正确的手机号'
                            }
                        }     
                        }}></input>
                    </span>
                </div>
                <div className='password'><input placeholder='请设置密码' valur={this.state.password} onChange={(e)=>{
                            this.setState({password:e.target.value})
                        }}></input></div>
                <div className='error1'></div>
                <div className='verifyCode'>
                     <span className='span1'>
                     <input placeholder='输入验证码' vlaue={this.state.verify} onChange={(e)=>{
                            this.setState({verify:e.target.value})
                        }}></input>
                     </span>
                    
                        {
                            this.state.time<10&&this.state.time>=0?
                            ( <span className='span2'>{this.state.time}s</span>)
                            :
                           (<span className='span2'>获取验证码</span>)
                        }
                    
                </div>
                <div className='error2'>
                    <span id='span1' className='span1'></span>
                    <span className='span22'>
                    <span className='span2'>你也可以接听</span>
                    <span className='span3'>语言验证码</span>
                    </span>
                </div>
                <div className='submit' onClick={()=>{
                   this.register()
                  
                }} >
                    注册
                </div>
                <div className='aaa' onClick={
                    ()=>{
                        this.props.history.push('/login')
                    }
                } >已注册，请直接登录</div>
            </div>
        )
    }
}
