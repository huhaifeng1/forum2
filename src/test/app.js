import React, { Component } from 'react'
import { HashRouter, Switch, Route } from 'react-router-dom'
import Register from './register/register'
import Person from './person/person'
import Login from './login/login'
import Article from './article/detail'

import ArticleList from './articleList/articleList'
let id = window.localStorage.getItem('id')
export const AuthContext = React.createContext({
    user: null,
    setUser: () => { }

})

export default class App extends Component {

    constructor(props) {
        super(props)

        this.setUser = (u) => {
            this.setState(
                { auth: { ...this.state.auth, user: u } }
            )
        }

        this.state = {
            auth: {
                user: id ? { 'id': id } : null,
                setUser: this.setUser
            }
        }
    }
    render() {
        return (
            <AuthContext.Provider value={this.state.auth}>
                <HashRouter>
                    <Switch>
                        <Route exact path='/' component={Register}></Route>
                        <Route path='/person' component={Person}></Route>
                        <Route path='/login' component={Login}></Route>
                        <Route path='/article' component={Article}></Route>
                        <Route path='/articleList' component={ArticleList}></Route>
                    </Switch>
                </HashRouter>
            </AuthContext.Provider>
        )
    }
}
