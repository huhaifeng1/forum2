import axios from 'axios'
import React, { Component } from 'react'
import './style.css'
import qs from 'qs'


export default class Login extends Component {
    state={
        phoneNum:'',
        verifyCode:'',
        url:'',
        verifyPic:'',
        key:'',
        password:'',
        verify:'',
        captcha_key:''
    }
    componentWillUnmount(){
        this.setState=(state,callback)=>{//清理state
            return
        }
    }
    login=()=>{
       
        let data={
            mobile:this.state.phoneNum,
            captcha_code:this.state.verifyCode,
            captcha_key:this.state.key,
            password:this.state.password
        }
        axios.post('http://playground.it266.com/api/user/token/mobile',qs.stringify(data) )
        .then((res)=>{
           
             if(!res.data.status){
                 alert(res.data.data)
             }else{
                 window.localStorage.setItem('token',res.data.data.token)
                 window.localStorage.setItem('id',res.data.data.id)
                this.props.history.push('/article')
             }
        })
        .catch((err)=>{
             console.log(err)
        })
    }
    loadPic=()=>{
       axios.get('http://playground.it266.com/api/captcha')
       .then((res)=>{
          
            if(!res.data.status){
                alert(res.data.data)
            }else{
                this.setState({
                    url:res.data.data.url,
                    key:res.data.data.key
                })
            }
       })
       .catch((err)=>{
            console.log(err)
       })
    }
    
    render() {
        return (
            <div className='Login' style={{backgroundColor:this.state.url===''?'white':'rgb(101, 101, 101)'}}>
                <div className='verifyPic' style={{display:this.state.url===''?"none":"block"}}>
                    <div className='verifyPic1'>安全验证</div>
                    <div className='verifyPic2'>
                        <img className='verifyPic21' src={this.state.url} alt=''></img>
                        <div className='verifyPic22'
                        onClick={()=>{
                            this.loadPic()
                        }}
                        >换一换</div>
                    </div>
                    <div className='verifyPic3'>
                        <input placeholder='请输入验证码' value={this.state.verifyCode} onChange={(e)=>{
                            this.setState({verifyCode:e.target.value})
                        }}></input>
                    </div>
                    <div className='verifyPic4' onClick={()=>{
                        
                       this.login()
                       this.setState({url:''})
                      
                    }}>登录</div>
                </div>

                <div className='head'>
                    <span className='span1'>&lt;</span>
                   
                </div>
                <div className='instruction'>登录</div>
               
                <div className='phoneNum'>
                    <span className='span1'>+86</span>
                    <span className='span2'>
                        <input placeholder='输入手机号' vlaue={this.state.verifyCode} onChange={(e)=>{
                            this.setState({phoneNum:e.target.value})
              
                            if(
                             (/^1[3456789]\d{9}$/).test(e.target.value)&&e.target.value.length===11&&this.state.password.length===6
                            ){
                                                              
                                       this.loadPic()
                          
                        }else{
                            if(e.target.value.length===11){
                                document.getElementsByClassName('error1')[0].innerHTML='请输入正确的手机号'
                            }
                        }     
                        }}></input>
                    </span>
                </div>
                <div className='password'><input placeholder='请输入密码' valur={this.state.password} onChange={(e)=>{
                            this.setState({password:e.target.value})
                        }}></input></div>
                <div className='error1'></div>
               
              
            </div>
        )
    }
}
