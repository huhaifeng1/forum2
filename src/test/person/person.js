import React, { Component } from 'react'
import axios from 'axios'
import './style.css'
import { AuthContext } from '../app'

let id=window.localStorage.getItem('id')

export default class Person extends Component {
    state = {
        mobile: '',
        nickname: '',
        id: '',
        created_at: ''
    }

    componentDidMount() {
        let token = window.localStorage.getItem('token')
        axios.get('http://playground.it266.com/api/user/profile?token=' + token)
            .then((res) => {
                console.log(res)
                if (!res.data.status) {
                    alert(res.data.data)
                } else {
                    this.setState({
                        mobile: res.data.data.mobile,
                        nickname: res.data.data.nickname,

                        created_at: res.data.data.created_at,

                    })
                }
            })
            .catch((err) => { console.log(err) })
    }
    render() {
        return (

            <AuthContext.Consumer>
                {
                    (value) => {
                        return(
                            <div className='person'>
                            <div className='person1'>个人信息</div>
                            <div className='person2'>头像</div>
                            {
                                value.user?
                                <div className='person3'>本人ID :  {value.user.id}</div>:
                                <div className='person3'>本人ID :  1234</div>
                            }

                            
                            <div className='person4'>性别</div>
                            <div className='person5'>地区</div>
                            <div className='person6'><input value={this.state.id} onChange={(e) => {
                                this.setState({ id: e.target.value })
                            }} ></input></div>
                            <div onClick={
                                        ()=>{
                                            console.log('value=',value)
                                     value.setUser({ id: this.state.id })
                                    }
                                
                            }>修改id</div>
                        </div>
                        )
                      
                    }
                }

            </AuthContext.Consumer>
        )
    }
}
