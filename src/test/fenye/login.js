import React, { Component } from 'react'
import axios from 'axios'
import qs from 'qs'


export default class Login extends Component {
    state={
        username:'',password:'',error:''
    }
    submit=()=>{
        let data={
            username:this.state.username,
            password:this.state.password
        }
       
       axios.post('http://playground.it266.com/login',qs.stringify(data))
       .then((response)=>{
            if(!response.data.status){
                this.setState({error:response.data.data})
            }else{
                
                let token=response.data.data.token
             
                window.localStorage.setItem('token',token)
               
                this.props.history.push('/article')
            }
       })
       .catch((error)=>{
           console.log(error)
       })
    }
    render() {
        return (
            <div>
                <h2>登录</h2>
                <div>  账号<input placeholder='请输入账号' value={this.state.username} onChange={
                    (e)=>{
                        this.setState({username:e.target.value})
                    }
                }></input></div>
                <div>  密码<input placeholder='请输入密码' value={this.state.password} onChange={
                    (e)=>{
                        this.setState({password:e.target.value})
                    }
                } ></input></div>
                <div style={{color:"red"}}>{this.state.error}</div>
                <div> <button onClick={
                    this.submit
                }>提交</button></div>
            </div>
        )
    }
}
