import React from 'react'
import Login from './login'

import Article from './article'

import { HashRouter, Switch, Route } from 'react-router-dom'
class Start extends React.Component {
    render() {
        return (
            <HashRouter>
                <Switch>

                    <Route exact path='/' component={Login}></Route>

                    <Route  path='/article' component={Article}></Route>

                </Switch>
            </HashRouter>

        )
    }
}

export default Start