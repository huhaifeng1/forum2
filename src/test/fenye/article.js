import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import './style.css'
export default class Article extends Component {
    state = {
        list: [], error: '',
        title: '',
        beginTime: '',
        endTime: '',
        currentPage: 1,
        page_size: 3,
        itemCount: 0,
    }
    componentDidMount() {
        this.getDate()
    }
    getDate = () => {
        let token = window.localStorage.getItem('token')

        axios.get('http://playground.it266.com/article', {
            params: {
                token: token,
                page: this.state.currentPage,
                page_size: this.state.page_size,
                title: this.state.title,
                created_begin: this.state.beginTime,
                created_end: this.state.endTime
            }
        })
            .then((response) => {
                console.log('res=', response)
                if (!response.data.status) {
                    alert(response.data.data)
                    return
                } else {

                    this.setState({
                        list: response.data.data.articles,
                        itemCount: response.data.data.page.itemCount,
                        currentPage: response.data.data.page.currentPage,

                    })
                }
            })
            .catch((error) => {
                console.log(error)
            })
    }
    showPage = () => {
        let many = Math.ceil(this.state.itemCount / this.state.page_size)


        let arr = []
        for (let i = 1; i <= many; i++) {
            arr.push(<span className={this.state.currentPage === i ? 'active' : ''} key={i} onClick={() => {
                this.setState({ currentPage: i }, this.getDate)
            }}>{i}</span>)
        }
        return (
            <div className='foot'>
                <div className='foot1'>共{this.state.itemCount}條</div>
                <div className='foot2'>{arr}</div>

                <div className='foot3'>
                    <select onChange={
                        (e) => {
                            this.setState({
                                page_size: e.target.value,
                                currentPage: 1
                            }, this.getDate)

                        }
                    }>
                        <option value='3'>3條/頁</option>
                        <option value='5'>5條/頁</option>
                        <option value='10'>10條/頁</option>
                    </select>
                </div>
                    跳至 <div className='foot4'>

                    <input onKeyDown={(e) => {

                        if (e.code === 'Enter' || e.code === 'NumpadEnter') {
                            this.setState({ currentPage: e.target.value }, this.getDate)
                        }
                    }} ></input>
                </div>页
            </div>

        )







    }
    render() {
        return (
            <div className='article'>
                <div className='guanli'>文章管理</div>
                <Link to="/article/create" className='xinzeng'>新增</Link>
                <div className='search'>
                    <div><input onChange={(e) => {
                        this.setState({
                            title: e.target.value
                        })
                    }} placeholder='文章標題' value={this.state.title}></input></div>

                    <div><input onChange={(e) => {
                        this.setState({
                            beginTime: e.target.value
                        })
                    }} placeholder='開始時間' value={this.state.beginTime}></input></div>
                    <div><input onChange={(e) => {
                        this.setState({
                            endTime: e.target.value
                        })
                    }} placeholder='結束時間' value={this.state.endTime}></input></div>
                    <div className='bu'>
                        <div onClick={

                            this.getDate
                        }>搜索</div>
                        <div onClick={() => {
                            this.setState({ title: '', beginTime: '', endTime: '' }, this.getDate)
                        }} >清空</div>
                    </div>
                </div>
                <div className='neck'>
                    <div className='id'>ID</div>
                    <div>標題</div>
                    <div>創建時間</div>
                    <div>操作</div>
                </div>
                {
                    this.state.list.map((item, i) => {
                        return (
                            <div className='body' key={i}>

                                <div>{item.id}</div>
                                <div> {item.title}</div>
                                <div>   {item.created_at}</div>
                                <div>       编辑  | 删除
                                </div>
                            </div>
                        )
                    })
                }
                {
                    this.showPage()
                }


            </div>
        )
    }
}
