import React, { useState,useContext } from 'react'

import axios from 'axios'
import { useHistory } from 'react-router'
import url from './common'
import qs from 'qs'


export default function SuperAdm() {
    let [username, setUsername] = useState('')
    let [password, setPassword] = useState('')
    
    let history=useHistory()

    function gotoHome() {
        let jsonData={
            username:username,
            password:password
        }
        axios.post(url+'/api/user/admLogin',qs.stringify(jsonData) )
        .then((res) => {
         
            if (res.data.code === 'SUCCESS') {
                window.localStorage.setItem('token', res.data.token)
                console.log("token=",res.data.token)
                history.push('/admLoadFile')
                
            } else {
                console.log(res)
               alert(res.data.message)
            }
        })
        .catch((err) => {
            console.log(err)
        }) 
    }

    return (
        <div className='login'>
          
            <div className='userName'>
               
                <div className='userName2'>
                用户名: <input placeholder='请输入您的用户名' onChange={(e) => {
                        setUsername(e.target.value)
                    }} ></input>
                </div>
                
            </div>
          
        
                <div className='password2'>
                密码: <input placeholder='请输入6到16位的密码' onChange={(e) => {
                        setPassword(e.target.value)
                    }} ></input>
                </div>
               
            
                <div className='submit' onClick={
                    gotoHome
                } >提交</div>
                
           
          
        </div>
                
    )
}
