import React, {  useState } from 'react'
import {HashRouter,Switch,Route} from 'react-router-dom'
import Home from './home/home'
import Top from './top/top'
import Login from './top/login/login'
import SuperAdm from './superAdm'
import AdmLoadFile from './admLoadFile'
import Register from './top/register/register'
import './style.css'
import PostList from './home/postList'
import PersonCenter from './home/personCenter/personCenter'


export const Context = React.createContext(null)//hooks跨组件通讯
                               
export default function App() {
    let nickname=window.localStorage.getItem('nickname')
    let avatar=window.localStorage.getItem('avatar')

   

    if(nickname==null){
        nickname='未登陆'
        avatar=''
    }
    const [n,setN]=useState({nickname:nickname,avatar:avatar})//hooks跨组件通讯

       
        return ( 
            <Context.Provider value={{n:n,setN:setN}}>
                <HashRouter>
                    <Top/>
                    <Switch>
                        <Route path='/post' component={Home}></Route>
                        <Route exact path='/' component={PostList}></Route>
                       <Route path='/login' component={Login}></Route> 
                       <Route path='/admin' component={SuperAdm}></Route>
                       <Route path='/admLoadFile' component={AdmLoadFile}></Route>
                       <Route path='/register' component={Register}></Route>
                       <Route path='/personCenter' component={PersonCenter}></Route> 
                    </Switch>
                </HashRouter>
                </Context.Provider>
        )
}

