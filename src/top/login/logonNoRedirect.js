import React, { useState,useContext } from 'react'
import './style.css'
import axios from 'axios'
import { useHistory } from 'react-router'
import { Context } from '../../app'
import url from '../../common'


export default function Login() {
    let [username, setUsername] = useState('')
    let [password, setPassword] = useState('')
    const context = useContext(Context)//hooks跨组件通讯

    let history = useHistory()
    function gotoHome() {
        let jsonData={
            username:username,
            password:password
        }
        axios.post(url+'/api/user/login',jsonData)
        .then((res) => {
         
            if (res.data.code === 'SUCCESS') {
                window.localStorage.setItem('token', res.data.token)
                window.localStorage.setItem('nickname', res.data.user.nickname)
                window.localStorage.setItem('avatar', res.data.user.avatar)
              context.setN({nickname:res.data.user.nickname,avatar:res.data.user.avatar})//hooks跨组件通讯
               
                
            } else {
               console.log(res)
               alert(res.data.message)
            }
        })
        .catch((err) => {
            console.log(err)
        }) 
    }

    // let history = useHistory();
    // function handleClick() {
    //   history.push("/articleList");
    // }
    return (
        <div className='login'>
            <div className='loginWay'>
                <div className='loginWay1'>密码登录</div>
                <div className='loginWay2'>邮箱登录</div>
                <div className='loginWay3'>扫码登录</div>
            </div>
            <div className='userName'>
               
                <div className='userName2'>
                用户名: <input placeholder='请输入您的用户名' onChange={(e) => {
                        setUsername(e.target.value)
                    }} ></input>
                </div>
                <div className='userName3'>使用第三方账号注册过的用户需先绑定手机/邮箱号</div>
            </div>
            <div className='password'>
               
                <div className='password2'>
                密码: <input placeholder='请输入6到16位的密码' onChange={(e) => {
                        setPassword(e.target.value)
                    }} ></input>
                </div>
                <div className='password3'>如果您之前未设置过密码，
                请使用其他方式登陆后在账户中心设置密码/邮箱号</div>
            </div>
            <div>
                <div className='submit' onClick={
                    gotoHome
                } >提交</div>
                <div>尚无账号？点击此处去注册</div>
            </div>
            <div>或者使用社交账号注册</div>
        </div>
                
    )
}
