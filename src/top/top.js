import React, {useContext } from 'react'
import './style.css'
import { Link } from 'react-router-dom'
import { Context } from '../app'

export default function Top() {
    //let token = window.localStorage.getItem('token')
    const context = useContext(Context)//hooks跨组件通讯
   // console.log('username=',context.n.username)


    // let [results, setResults] = useState([])
    // let [page, setPage] = useState(1)
    // useEffect(() => {
    //     axios.get("http://192.168.1.254:8081/api/post/list?limit=5&page=" + page)
    //         .then((res) => {
    //             //console.log(res)
    //             if (res.data.code === 'SUCCESS') {
    //                 setResults(res.data.data.postDtoList)
    //             } else {
    //                 console.log(res)
    //             }
    //         })
    //         .catch((err) => {
    //             console.log(err)
    //         })
    //     return () => {

    //     }
    // }, [])

    return (

        <div className='head'>
            <div className='headLeft'>
                <div className='headLeft1'>学坝</div>
                <div className='headLeft2'>
                    <Link className='link' to='/'>主页</Link>
                </div>
                <div className='headLeft3'>文档</div>
            </div>
            <div className='headRight'>

                {context.n.nickname === '未登陆' ? 
                    <>
                        
                    <div className='headRight1'></div>
                        <div className='headRight2'>
                            <Link className='link' to='/login'>登录</Link>
                        </div>
                        <div className='headRight3'>
                        <Link className='link' to='/register'>注册</Link>
                        </div>
                       </>:
                     <>  <div className='headRight1'>
                         
                         <Link className='link' to='/personCenter'>
                         <div className='avatar' style={{backgroundImage:`url(${context.n.avatar})`}} ></div>
                         </Link>
                         </div>
                        <div className='headRight2'>
                        <Link className='link' to='/personCenter'>
                        {context.n.nickname}
                         </Link>
                       
                        </div>
                        <div className='headRight3'></div>
             </>  }

            </div>
        </div>
    )
}
