import React, { useState,useContext,useEffect } from 'react'
import './style.css'
import axios from 'axios'
import { useHistory } from 'react-router'
import { Context } from '../../app'
import url from '../../common'


export default function Rigister() {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [nickname, setNickname] = useState('')
    const [mail, setMail] = useState('')
    const [passwordaga, setPasswordaga] = useState('')
    const [avatarList, setavatarList] = useState([])//注册时可选头像
    const [myAvatar, setmyAvatar] = useState(0)
   

    const context = useContext(Context)//hooks跨组件通讯

    let history = useHistory()

    useEffect(() => {
        axios.get(url+'/api/image/download')
        .then((res)=>{
          console.log(res)
            if(res.data.data.code==="SECCESS"){    
                setavatarList(res.data.data.data)
                setmyAvatar(res.data.data.data[0].id)
            }else{
                console.log(res)
            }
            
        })
        .catch((err)=>{
            console.log(err)
        })

        return () => {
            
        }
    }, [])

    function gotoHome() {
        // axios.post('http://192.168.1.254:8081/api/user/create',{params:{
        //     nickname:nickname,
        //     password:password,
        //     username:username
        // }})

        // axios.post('http://192.168.1.254:8081/api/user/create',data)
        let jsonData={
            username:username,
            nickname:nickname,
            password:password,
            avatarId:myAvatar,
            mail:mail
        }
        axios.post(url+'/api/user/registe',jsonData)
        .then((res) => {
            console.log(res)
           
            if (res.data.code === 'SUCCESS') {
               
                window.localStorage.setItem('token', res.data.token)
              context.setN({nickname: res.data.user.nickname,avatar:res.data.user.avatar})//hooks跨组件通讯
              window.localStorage.setItem('nickname', res.data.user.nickname)
              window.localStorage.setItem('avatar', res.data.user.avatar)
                history.push('/')
            } else {
                alert(res.data.message)
            }
        })
        .catch((err) => {
           
            console.log(err)
        })
  
        
    }

    // let history = useHistory();
    // function handleClick() {
    //   history.push("/articleList");
    // }
    return (
       
        <div className='register'>
            <div style={{display:'flex'}}>
                选择头像
                {
                    
                    avatarList.map((avatar)=>{
                        return (
                            <div onClick={()=>{
                                setmyAvatar(avatar.id)
                                console.log(avatar.id)
                            }} className={avatar.id===myAvatar?'hhfAvatarBlack':'hhfAvatarWhite'} key={avatar.id} style={{backgroundImage:`url(${avatar.avatar})`}}>
                            </div>
                        )
                    })
                }
            </div>
            <div className='loginWay'>
                <div className='loginWay1'>用户名注册</div>
            </div>
            <div className='userName'>
                <div className='userName1'>用户名</div>
                <div className='userName2'>
                    <input placeholder='请输入您的用户名' onChange={(e) => {
                        setUsername(e.target.value)
                    }} ></input>
                </div>
                
            </div>
            <div className='nickName'>
                <div className='nickName1'>昵称</div>
                <div className='nickName2'>
                    <input placeholder='请输入您的昵称' onChange={(e) => {
                        setNickname(e.target.value)
                    }} ></input>
                </div>
               
            </div>
            <div className='nickName'>
                <div className='nickName1'>邮箱</div>
                <div className='nickName2'>
                    <input placeholder='请输入您的昵称' onChange={(e) => {
                        setMail(e.target.value)
                    }} ></input>
                </div>
               
            </div>
            <div className='password'>
                <div className='password1'>设置密码</div>
                <div className='password2'>
                    <input placeholder='请输入6到16位的密码' onChange={(e) => {
                        setPassword(e.target.value)
                    }} ></input>
                </div>
            </div>
            <div className='passwordAgain'>
                <div className='passwordAgain1'>确认密码</div>
                <div className='passwordAgain2'>
                    <input placeholder='请再次输入您的密码' onChange={(e) => {
                        setPasswordaga(e.target.value)
                    }} ></input>
                </div>
            </div>
            <div>
                <div className='submit' onClick={
                    gotoHome
                } >提交</div>
                <div>已有账号？点击此处去登陆</div>
            </div>
            <div>或者使用社交账号注册</div>
        </div>
                
    )
}
